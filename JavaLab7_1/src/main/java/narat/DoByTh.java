package narat;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

public class DoByTh {

    private Thread[] thread;
    private String path;
    private String[] args;

    public DoByTh(String path, Thread[] thread, String[] args) throws InterruptedException {
        this.thread = thread;
        this.path = path;
        this.args = args;
    }

    public void doThread() throws InterruptedException {
        thread = new DThread[args.length];

        for (int i = 0; i < args.length; i++) {
            String[] filesName = new File(path).list();
            String fileName = checkFilesName(filesName, args[i]);
            thread[i] = new DThread(args[i], fileName, path);
            thread[i].start();
        }

        for (int i = 0; i < args.length; i++) {
            thread[i].join();
        }
    }

    private static String splitUrl(String url) {
        String s[] = url.split("/");
        return s[s.length - 1];
    }

    private static String checkFilesName(String[] filesName, String args) {
        ArrayList arrayList = new ArrayList();
        Collections.addAll(arrayList, filesName);
        Collections.sort(arrayList);
        StringBuffer fileName = new StringBuffer(splitUrl(args));
        for (int j = 0; j < filesName.length; j++) {
            //System.out.println(fileName);
            //System.out.println(new StringBuffer(arrayList.get(j).toString()));
            StringBuffer stringBuffer = new StringBuffer(arrayList.get(j).toString());
            if (fileName.toString().equals(stringBuffer.toString())) {
                fileName = fileName.insert(0, j);
            }
        }
        return fileName.toString();
    }
}

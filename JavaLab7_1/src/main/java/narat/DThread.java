package narat;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;

public class DThread extends Thread {

    private String url;
    private String fileName;
    private String path;


    DThread(String url, String fileName, String path){
        this.url = url;
        this.fileName = fileName;
        this.path = path;
    }

    public void run(){
        try {
            downloadUsingStream(url, fileName, path);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void downloadUsingStream(String urlStr, String fileName, String path) throws IOException {
        URL url = new URL(urlStr);
        InputStream inputStream = url.openStream();
        Files.copy(inputStream, new File(path + fileName).toPath());
    }
}
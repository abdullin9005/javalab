<%@ page import="java.util.List" %>
<%@ page import="models.Product" %><%--
  Created by IntelliJ IDEA.
  User: abdul
  Date: 20.12.2019
  Time: 20:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Продукты</title>

</head>
<body>
<ul>
    <%
        List<Product> products = (List<Product>) request.getAttribute("products");

        if (products != null && !products.isEmpty()) {
            for (Product s : products) {
                out.println("<li>" + s.getName() + " " + s.getPrice() + "</li>");
            }
        }
    %>
</ul>
</body>
</html>

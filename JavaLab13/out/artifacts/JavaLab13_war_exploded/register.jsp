<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Registration</title>
</head>

<body>
<div class="form-container">
    <form method="post">
        <h2 class="text-center"><strong>Регистрация</strong>.</h2>
        <div class="form-group"><input class="form-control" type="text" name="username" placeholder="username"></div>
        <div class="form-group"><input class="form-control" type="password" name="password" placeholder="Пароль">
        </div>


        <div class="form-group">
            <button class="btn btn-primary btn-block" type="submit">Зарегистрироваться</button>
        </div>
    </form>
</div>

</body>

</html>

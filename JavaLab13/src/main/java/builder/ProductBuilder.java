package builder;

import models.Product;

public interface ProductBuilder extends Builder<Product> {
    void setName(String name);
    void setPrice(String price);
}

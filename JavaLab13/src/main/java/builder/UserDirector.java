package builder;


import models.User;

public class UserDirector {
    private UserBuilderImpl builder;

    public UserDirector(UserBuilderImpl builder) {
        this.builder = builder;
    }

    public User createUserWOId(String username, String password) {
        builder.reset();
        builder.setUsername(username);
        builder.setPassword(password);
        return builder.getResult();
    }
    public User createFullUser(Integer id, String username, String password) {
        builder.reset();
        builder.setId(id);
        builder.setUsername(username);
        builder.setPassword(password);
        return builder.getResult();
    }
}

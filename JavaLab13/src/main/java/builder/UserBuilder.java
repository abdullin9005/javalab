package builder;

import models.User;

public interface UserBuilder extends Builder<User> {
    void setId(Integer id);
    void setUsername(String username);
    void setPassword(String password);
}

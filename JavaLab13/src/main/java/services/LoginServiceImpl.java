package services;

import context.Component;
import dto.Login;
import repositories.UsersRepositoryImpl;

public class LoginServiceImpl implements LoginService, Component {

    UsersRepositoryImpl usersRepository;

    @Override
    public Boolean login(Login login) {
        return usersRepository.find(login);
    }
}

package services;

import dto.Login;

public interface LoginService {
    Boolean login(Login login);
}

package services;

import context.Component;
import models.Product;
import repositories.ProductsRepositoryImpl;

import java.util.List;

public class ProductServiceImpl implements ProductService, Component {

    ProductsRepositoryImpl productsRepository;

    @Override
    public List<Product> getProducts() {
        return productsRepository.findAll();
    }
}

package services;

import context.Component;
import dto.Login;
import repositories.UsersRepositoryImpl;

public class RegisterServiceImpl implements RegisterService, Component {

    UsersRepositoryImpl usersRepository;

    @Override
    public void addUser(Login login) {
        usersRepository.add(login);
    }
}

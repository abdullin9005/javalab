package contexts;

import context.Component;
import services.*;

public class PrimitiveContext implements Component {

    LoginServiceImpl loginService;
    ProductServiceImpl productService;
    RegisterServiceImpl registerService;

    public LoginService loginService() {
        return loginService;
    }

    public ProductService productService(){
        return productService;
    }

    public RegisterService registerService(){
        return registerService;
    }
}

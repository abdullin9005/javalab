package dto;

public class UserDto {
    private long id;

    public UserDto() {
    }

    public UserDto(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}

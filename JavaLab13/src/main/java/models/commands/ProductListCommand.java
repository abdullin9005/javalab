package models.commands;

public class ProductListCommand implements Command {
    private String command;

    public ProductListCommand() {
    }

    public ProductListCommand(String command) {
        this.command = command;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }
}

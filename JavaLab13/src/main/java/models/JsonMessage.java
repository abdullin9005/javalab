package models;

public class JsonMessage {
    private String header;
    private Object payload;

    public JsonMessage(String header, Object payload) {
        this.header = header;
        this.payload = payload;
    }

    public JsonMessage() {
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public Object getPayload() {
        return payload;
    }

    public void setPayload(Object payload) {
        this.payload = payload;
    }
}

package repositories;

import builder.UserBuilderImpl;
import builder.UserDirector;
import context.Component;
import dto.Login;
import models.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class UsersRepositoryImpl implements UsersRepository, Component {
    private UserDirector director = new UserDirector(new UserBuilderImpl());
    private DBConnection dbConnection = new DBConnection();
    private Connection connection = dbConnection.getConnection();
    private PasswordEncoder encoder = new BCryptPasswordEncoder();

    @Override
    public Optional<User> findOneByUsername(String username) {
        try {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM db4.user WHERE login = ?");
            statement.setString(1, username);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return Optional.of(director.createFullUser(resultSet.getInt("id"), resultSet.getString("username"), resultSet.getString("password")));
            } else {
                return Optional.empty();
            }
        } catch (SQLException e) {
            System.out.println();
            throw new IllegalArgumentException();
        }
    }

    @Override
    public Boolean find(Login login) {
        try {
        PreparedStatement stmt = connection.prepareStatement("SELECT * FROM db4.user WHERE login = ?");
        stmt.setString(1, login.getUsername());
        ResultSet rs = stmt.executeQuery();
        if (rs.next()) {
            String hash = rs.getString("hash");
            if (encoder.matches(login.getPassword(), hash)) {
                return true;
            } else {
                return false;
            }
        } else {
            return null;
        }
    } catch (SQLException e) {
        System.out.println("Error during finding user");
        throw new IllegalArgumentException(e);
    }
    }

    @Override
    public Optional<User> findOne(Integer id) {
        try {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM db4.user WHERE id = ?");
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return Optional.of(director.createFullUser(resultSet.getInt("id"), resultSet.getString("username"), resultSet.getString("password")));
            } else {
                return Optional.empty();
            }
        } catch (SQLException e) {
            System.out.println();
            throw new IllegalArgumentException();
        }
    }

    @Override
    public List<User> findAll() {
        ArrayList<User> users = new ArrayList<>();
        try {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM db4.user");
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                users.add(director.createFullUser(resultSet.getInt("id"), resultSet.getString("username"), resultSet.getString("password")));
            }
        } catch (SQLException e) {
            System.out.println();
            throw new IllegalArgumentException();
        }
        return users;
    }

    @Override
    public void add(User user) {
        return;
    }

    @Override
    public void add(Login user) {
        try {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO db4.user (login, hash) VALUES (?, ?)");
            statement.setString(1, user.getUsername());
            statement.setString(2, encoder.encode(user.getPassword()));
            statement.executeUpdate();
        } catch (SQLException e) {
            System.out.println();
            throw new IllegalArgumentException();
        }
    }

    @Override
    public void removeOne(Integer id) {
        try {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM db4.user WHERE id = ?");
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            System.out.println();
            throw new IllegalArgumentException();
        }
    }
}

package repositories;

import dto.Login;
import models.User;

import java.util.Optional;

public interface UsersRepository extends CrudRepository<User, Integer> {
    Optional<User> findOneByUsername(String username);
    Boolean find(Login login);

    void add(Login user);
}

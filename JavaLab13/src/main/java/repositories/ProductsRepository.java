package repositories;

import models.Product;

public interface ProductsRepository extends CrudRepository<Product, Integer> {
}

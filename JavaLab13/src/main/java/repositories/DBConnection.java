package repositories;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DBConnection {
    private Connection connection;
    private String dbPropertiesFilePath = "D:\\JavaLab13\\src\\main\\java\\db.properties";

    public DBConnection() {
        try {
            FileInputStream fileInputStream = new FileInputStream(new File(dbPropertiesFilePath));
            Properties properties = new Properties();
            properties.load(fileInputStream);
            String user = properties.getProperty("user");
            String password = properties.getProperty("password");
            String url = properties.getProperty("url");
            Class.forName("com.mysql.cj.jdbc.Driver");
            connection = DriverManager.getConnection(url, user, password);
        } catch (SQLException e) {
            System.out.println("Cant get connection.");
            throw new IllegalArgumentException(e);
        } catch (ClassNotFoundException e) {
            System.out.println("Cant find driver class");
            throw new IllegalArgumentException(e);
        } catch (IOException e) {
            System.out.println("Cant get access to properties file");
            throw new IllegalStateException(e);
        }
    }

    public Connection getConnection() {
        return connection;
    }
}


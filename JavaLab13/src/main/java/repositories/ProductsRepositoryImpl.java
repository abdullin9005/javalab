package repositories;

import builder.ProductBuilderImpl;
import builder.ProductDirector;
import context.Component;
import models.Product;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class ProductsRepositoryImpl implements ProductsRepository, Component {
    private ProductDirector director = new ProductDirector(new ProductBuilderImpl());
    private DBConnection dbConnection = new DBConnection();
    private Connection connection = dbConnection.getConnection();

    @Override
    public Optional<Product> findOne(Integer integer) {
        return Optional.empty();
    }

    @Override
    public List<Product> findAll() {
        try {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM db4.products");
            ResultSet resultSet = statement.executeQuery();
            LinkedList<Product> products = new LinkedList<>();
            while (resultSet.next()) {
                products.add(director.createFullProduct(resultSet.getString("name"), resultSet.getString("price")));
            }
            return products;
        } catch (SQLException e) {
            System.out.println("Exception during product getting");
            throw new IllegalArgumentException();
        }
    }

    @Override
    public void add(Product product) {
        try {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO db4.products (name, price) VALUES (?, ?)");
            statement.setString(1, product.getName());
            statement.setString(2, product.getPrice());
            statement.executeUpdate();
        } catch (SQLException e) {
            System.out.println("Adding is not complete");
            throw new IllegalArgumentException();
        }
    }

    @Override
    public void removeOne(Integer integer) {

    }
}

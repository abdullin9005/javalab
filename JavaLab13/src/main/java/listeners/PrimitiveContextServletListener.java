package listeners;

import context.ApplicationContextReflectionBased;
import contexts.PrimitiveContext;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;


@WebListener
public class PrimitiveContextServletListener implements ServletContextListener {

    public void contextInitialized(ServletContextEvent servletContextEvent) {
        System.out.println("IN LISTENER");
        ApplicationContextReflectionBased contextReflectionBased = new ApplicationContextReflectionBased();
        ServletContext servletContext = servletContextEvent.getServletContext();
        PrimitiveContext primitiveContext = (PrimitiveContext) contextReflectionBased.getComponent(PrimitiveContext.class);
        servletContext.setAttribute("componentsContext", primitiveContext);
    }

    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }
}
package filters;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

public class LogFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpReq = (HttpServletRequest) servletRequest;
        String method = httpReq.getMethod();
        String ip = httpReq.getRemoteAddr();
        String date = new java.util.Date().toString();
        File file = new File("log.txt");
        PrintWriter pw = new PrintWriter(file);
        pw.append(date);
        pw.append(method);
        pw.append(ip);
        pw.flush();

        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy(){
    }
}

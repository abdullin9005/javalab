package servlets;

import narat.DThread;
import narat.DoByTh;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/download")
public class DownloadingServlet extends HttpServlet {
    private String html = "<!DOCTYPE html>\n" +
            "<html>\n" +
            "\t<head>\n" +
            "\t\t<title>Пример</title>\n" +
            "\t</head>\n" +
            "\t<body>\n" +
            "\t\t<form action=\"download\" method=\"POST\">\n" +
            "\t\t\tLogin: <input type = \"text\"  name = \"URL\" placeholder = \"Введите URL'ы через пробел\"><br><br>\n" +
            "\t\t\t<button type = \"submit\">Скачать</button>\n" +
            "\t\t</form>\n" +
            "\t</body>\n" +
            "</html>";

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        createPage(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        String[] urls = request.getParameter("URL").split(" ");
        Thread[] thread = new DThread[1];
        String path = "d:/Новая папка/";
        DoByTh doByTh = null;
        try {
            doByTh = new DoByTh(path, thread, urls);
            doByTh.doThread();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void createPage(HttpServletRequest request, HttpServletResponse response) throws IOException {
        PrintWriter writer = response.getWriter();
        writer.println(html);
    }
}

package models.commands;

public class GetMessagesCommand implements Command {
    private String command;
    private Integer page;
    private Integer size;
    private String token;

    public GetMessagesCommand(String command, Integer page, Integer size, String token) {
        this.command = command;
        this.page = page;
        this.size = size;
        this.token = token;
    }

    public GetMessagesCommand() {
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}

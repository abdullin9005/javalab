package components.handlers;

import builder.MessageBuilderImpl;
import builder.MessageDirector;
import context.Component;
import models.Message;
import components.repositories.MessagesRepositoryImpl;

import java.sql.SQLException;
import java.util.List;

public class MessageInfoHandler implements Component {
    private MessagesRepositoryImpl repository;
    private MessageDirector director = new MessageDirector(new MessageBuilderImpl());

    public List<Message> getMessages(int limit, int offset) {
        try {
            return getMessages0(limit, offset);
        } catch (SQLException e) {
            System.out.println();
            throw new IllegalArgumentException();
        }
    }

    private List<Message> getMessages0(int limit, int offset) throws SQLException {
        return repository.getMessagesOnPage(limit, offset);
    }

    public void addMessage(Integer id, String text) {
        repository.add(director.createFullMessage(id, text));
    }
}

package components.handlers;

import builder.UserBuilderImpl;
import builder.UserDirector;
import context.Component;
import models.User;
import components.repositories.UsersRepositoryImpl;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.List;

public class UserInfoHandler implements Component {
    private UsersRepositoryImpl repository;
    private UserDirector director = new UserDirector(new UserBuilderImpl());

    public boolean checkUser(String username, String password) {
        PasswordEncoder encoder = new BCryptPasswordEncoder();
        List<User> users = repository.findAll();
        for (User user : users) {
            if (user.getUsername().equals(username) && encoder.matches(password, user.getPassword())) {
                return true;
            }
        }
        return false;
    }

    public int getIdByUsername(String username) {
        return repository.findOneByUsername(username).get().getId();
    }
    public String getRoleByUsername(String username) {
        return repository.findOneByUsername(username).get().getRole();
    }

    public void createUser(String username, String password, String role) {
        repository.add(director.createUserWOId(username, password, role));
    }
}

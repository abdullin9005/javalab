package components.handlers;

import builder.ProductBuilderImpl;
import builder.ProductDirector;
import context.Component;
import models.Product;
import components.repositories.ProductsRepositoryImpl;

import java.sql.SQLException;
import java.util.List;

public class ProductInfoHandler implements Component {
    private ProductsRepositoryImpl repository;
    private ProductDirector director = new ProductDirector(new ProductBuilderImpl());

    public List<Product> getProducts() {
        try {
            return getProducts0();
        } catch (SQLException e) {
            System.out.println(); // TODO: 06.11.2019
            throw new IllegalArgumentException();
        }
    }

    private List<Product> getProducts0() throws SQLException {
        return repository.findAll();
    }
    public void addProduct(String name, String price) {
        repository.add(director.createFullProduct(name, price));
    }
}

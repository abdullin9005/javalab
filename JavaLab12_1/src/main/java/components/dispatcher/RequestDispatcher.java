package components.dispatcher;

import com.fasterxml.jackson.databind.ObjectMapper;
import context.Component;
import components.handlers.MessageInfoHandler;
import components.handlers.ProductInfoHandler;
import components.handlers.UserInfoHandler;
import models.JsonMessage;
import components.server.ChatMultiServer;
import components.services.JsonMessageCreator;
import components.services.TokenHandler;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.*;

public class RequestDispatcher implements Component {
    private UserInfoHandler userInfoHandler;
    private MessageInfoHandler messageInfoHandler;
    private ProductInfoHandler productInfoHandler;
    private TokenHandler tokenHandler = new TokenHandler();
    private ObjectMapper mapper = new ObjectMapper();
    private JsonMessageCreator messageCreator = new JsonMessageCreator();

    public void doDispatch(String jsonMessage, Socket clientSocket, BufferedReader input, List<ChatMultiServer.ClientHandler> clients, String username) {
        try {
            doDispatch0(jsonMessage, clientSocket, input, clients, username);
        } catch (IOException e) {
            System.out.println();
            throw new IllegalArgumentException();
        }
    }

    private void doDispatch0(String jsonMessage, Socket clientSocket, BufferedReader input, List<ChatMultiServer.ClientHandler> clients, String username) throws IOException {
        JsonMessage message = mapper.readValue(jsonMessage, JsonMessage.class);
        String header = message.getHeader();
        LinkedHashMap payload = (LinkedHashMap) message.getPayload();

        switch (header) {
            case "Logout":
                input.close();
                clientSocket.close();
                break;
            case "Command":
                String token = (String) payload.get("token");
                Claims tokenClaims = tokenHandler.getTokenClaims(token);
                PrintWriter output = new PrintWriter(clientSocket.getOutputStream(), true);
                String commandType = (String) payload.get("command");
                switch (commandType) {
                    case "getMessages":
                        if (tokenClaims.get("role", String.class).equals("admin")) {
                            Integer page = (Integer) payload.get("page");
                            Integer size = (Integer) payload.get("size");
                            output.println(messageCreator.getMessages(messageInfoHandler.getMessages(size, page), page, size));
                            break;
                        } else {
                            output.println("Access denied");
                            break;
                        }

                    case "getProducts":
                        output.println(messageCreator.getProducts(productInfoHandler.getProducts()));
                        break;
                    case "addProduct":
                        if (tokenClaims.get("role", String.class).equals("admin")) {
                            String name = (String) payload.get("name");
                            String price = (String) payload.get("price");
                            productInfoHandler.addProduct(name, price);
                            break;
                        } else {
                            output.println("Access denied");
                            break;
                        }
                }
                break;
            case "Message":
                for (ChatMultiServer.ClientHandler client : clients) {
                    if (client.clientSocket.isClosed()) {
                        clients.remove(client);
                        continue;
                    }
                    PrintWriter outTemp = new PrintWriter(client.clientSocket.getOutputStream(), true);
                    String text = (String) payload.get("text");
                    outTemp.println("[Server] " + username + ": \"" + text + "\"");
                }
                messageInfoHandler.addMessage(userInfoHandler.getIdByUsername(username), (String) payload.get("text"));
                break;
        }
    }
    public String authorization(Socket clientSocket, BufferedReader input) {
        try {
            PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
            JsonMessage message = mapper.readValue(input.readLine(), JsonMessage.class);
            LinkedHashMap map = (LinkedHashMap) message.getPayload();
            String username = (String) map.get("username");

            if (userInfoHandler.checkUser(username, (String) map.get("password"))) {
                Map<String, Object> tokenData = new HashMap<>();
                tokenData.put("userId", userInfoHandler.getIdByUsername(username));
                tokenData.put("role", userInfoHandler.getRoleByUsername(username));
                JwtBuilder jwtBuilder = Jwts.builder();
                jwtBuilder.setClaims(tokenData);
                String token = jwtBuilder.signWith(tokenHandler.getKey()).compact();
                out.println("@Server: You are in the chat as " + username);
                out.println(token);
                return username;
            } else {
                return null;
            }
        } catch (IOException e) {
            System.out.println("Fail in authorization");
            throw new IllegalArgumentException();
        }
    }
}

package components.services;

import context.Component;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import org.springframework.security.authentication.AuthenticationServiceException;

import javax.crypto.SecretKey;

public class TokenHandler implements Component {

    private SecretKey key = Keys.secretKeyFor(SignatureAlgorithm.HS256);

    public SecretKey getKey() {
        return key;
    }

    public void setKey(SecretKey key) {
        this.key = key;
    }

    public Claims getTokenClaims(String token) {
        Claims claims;
        try {
            claims = (Claims) Jwts.parser().setSigningKey(key).parse(token).getBody();
        } catch (Exception ex) {
            throw new AuthenticationServiceException("Token corrupted");
        }
        return claims;
    }
}

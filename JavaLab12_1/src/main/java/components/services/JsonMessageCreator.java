package components.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import context.Component;
import models.Message;
import models.Messages;
import models.Product;
import models.Products;

import java.util.List;

public class JsonMessageCreator implements Component {
    public String getProducts(List<Product> data) {
        ObjectMapper mapper = new ObjectMapper();
        Products products = new Products();
        products.setData(data);
        String jsonString;
        try {
            jsonString = mapper.writeValueAsString(products);
        } catch (JsonProcessingException e) {
            System.out.println();
            throw new IllegalArgumentException();
        }
        return jsonString;
    }

    public String getMessages(List<Message> data, Integer page, Integer size) {
        ObjectMapper mapper = new ObjectMapper();
        Messages messages = new Messages();
        messages.setData(data);
        String jsonString;
        try {
            jsonString = mapper.writeValueAsString(messages);
        } catch (JsonProcessingException e) {
            System.out.println();
            throw new IllegalArgumentException();
        }
        return jsonString;
    }
}

package components.server;

import context.Component;
import components.dispatcher.RequestDispatcher;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

public class ChatMultiServer implements Component {
    // список клиентов
    private List<ClientHandler> clients;
    private RequestDispatcher dispatcher;

    public ChatMultiServer() {
        // Список для работы с многопоточностью
        clients = new CopyOnWriteArrayList<>();
    }

    public void start(int port) {
        try {
            start0(port);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    private void start0(int port) throws IOException {
        ServerSocket serverSocket;
        serverSocket = new ServerSocket(port);
        while (true) {
            new ClientHandler(serverSocket.accept()).start();
        }
    }

    public class ClientHandler extends Thread {
        public Socket clientSocket;
        private BufferedReader input;
        private String username;

        ClientHandler(Socket socket) {
            this.clientSocket = socket;
            try {
                input = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            } catch (IOException e) {
                System.out.println(); // TODO: 13.11.2019
                throw new IllegalArgumentException();
            }
            // если авторизовался добавляем текущее подключение в список
            if ((username = dispatcher.authorization(clientSocket, input)) != null) {
                clients.add(this);
                System.out.println("New client");
            } else {
                try {
                    clientSocket.close();
                } catch (IOException e) {
                    System.out.println("Exception during closing");
                    throw new IllegalArgumentException();
                }
                System.out.println("Failed client connection");
            }
        }

        public void run() {
            if (!clientSocket.isClosed()) {
                try {
                    // получем входной поток для конкретного клиента
                    input = new BufferedReader(
                            new InputStreamReader(clientSocket.getInputStream()));
                    String inputLine;
                    while (!clientSocket.isClosed() && (inputLine = input.readLine()) != null) {
                        dispatcher.doDispatch(inputLine, clientSocket, input, clients, username);
                    }
                    input.close();
                    clientSocket.close();
                } catch (Exception e) {
                    throw new IllegalStateException(e);
                }
            }
        }
    }
}
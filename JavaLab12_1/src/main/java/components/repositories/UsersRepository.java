package components.repositories;

import models.User;

import java.util.Optional;

public interface UsersRepository extends CrudRepository<User, Integer> {
    Optional<User> findOneByUsername(String username);
}

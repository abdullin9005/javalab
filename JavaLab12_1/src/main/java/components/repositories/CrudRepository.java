package components.repositories;

import java.util.List;
import java.util.Optional;

public interface CrudRepository<T, ID> {
    Optional<T> findOne(ID id);
    List<T> findAll();
    void add(T t);
    void removeOne(ID id);
}

package components.repositories;

import builder.MessageBuilderImpl;
import builder.MessageDirector;
import context.Component;
import models.Message;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class MessagesRepositoryImpl implements MessagesRepository, Component {
    private MessageDirector director = new MessageDirector(new MessageBuilderImpl());
    private DBConnection dbConnection = new DBConnection();
    private Connection connection = dbConnection.getConnection();

    @Override
    public List<Message> getMessagesOnPage(Integer limit, Integer offset) {
        try {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM chat.messages LIMIT ? OFFSET ?");
            statement.setInt(1, limit);
            statement.setInt(2, (offset - 1) * 20);
            ResultSet resultSet = statement.executeQuery();
            LinkedList<Message> messages = new LinkedList<>();
            while (resultSet.next()) {
                messages.add(director.createFullMessage(resultSet.getInt("user_id"),  resultSet.getString("text")));
            }
            return messages;
        } catch (SQLException e) {
            System.out.println("Exception during message getting");
            throw new IllegalArgumentException();
        }
    }

    @Override
    public Optional<Message> findOne(Integer integer) {
        return Optional.empty();
    }

    @Override
    public List<Message> findAll() {
        return null;
    }

    @Override
    public void add(Message message) {
        try {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO chat.messages (text, user_id, date) VALUES (?, ?, ?)");
            statement.setString(1, message.getText());
            statement.setInt(2, message.getWriterId());
            statement.setString(3, new Date().toString());
            statement.executeUpdate();
        } catch (SQLException e) {
            System.out.println("Adding is not complete");
            throw new IllegalArgumentException();
        }
    }

    @Override
    public void removeOne(Integer integer) {

    }
}

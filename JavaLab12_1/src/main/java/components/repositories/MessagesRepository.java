package components.repositories;

import models.Message;

import java.util.List;

public interface MessagesRepository extends CrudRepository<Message, Integer> {
    List<Message> getMessagesOnPage(Integer limit, Integer offset);
}

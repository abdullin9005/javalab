package components.repositories;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {

    public Connection getConnection() {
        try {
            return DriverManager.getConnection("jdbc:mysql://localhost:3306", "root", "admin");
        } catch (SQLException e) {
            System.out.println();
            throw new IllegalArgumentException();
        }
    }
}

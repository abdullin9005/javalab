package components.repositories;

import builder.UserBuilderImpl;
import builder.UserDirector;
import context.Component;
import models.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class UsersRepositoryImpl implements UsersRepository, Component {
    private UserDirector director = new UserDirector(new UserBuilderImpl());
    private DBConnection dbConnection = new DBConnection();
    private Connection connection = dbConnection.getConnection();

    @Override
    public Optional<User> findOneByUsername(String username) {
        try {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM chat.user WHERE username = ?");
            statement.setString(1, username);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return Optional.of(director.createFullUser(resultSet.getInt("id"), resultSet.getString("username"), resultSet.getString("password"), resultSet.getString("role")));
            } else {
                return Optional.empty();
            }
        } catch (SQLException e) {
            System.out.println(); // TODO: 12.11.2019
            throw new IllegalArgumentException();
        }
    }

    @Override
    public Optional<User> findOne(Integer id) {
        try {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM chat.user WHERE id = ?");
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return Optional.of(director.createFullUser(resultSet.getInt("id"), resultSet.getString("username"), resultSet.getString("password"), resultSet.getString("role")));
            } else {
                return Optional.empty();
            }
        } catch (SQLException e) {
            System.out.println(); // TODO: 12.11.2019
            throw new IllegalArgumentException();
        }
    }

    @Override
    public List<User> findAll() {
        ArrayList<User> users = new ArrayList<>();
        try {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM chat.user");
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                users.add(director.createFullUser(resultSet.getInt("id"), resultSet.getString("username"), resultSet.getString("password"), resultSet.getString("role")));
            }
        } catch (SQLException e) {
            System.out.println(); // TODO: 12.11.2019
            throw new IllegalArgumentException();
        }
        return users;
    }

    @Override
    public void add(User user) {
        try {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO chat.user (username, password) VALUES (?, ?, ?, ?)");
            statement.setInt(1, user.getId());
            statement.setString(2, user.getUsername());
            statement.setString(3, user.getPassword());
            statement.setString(4, user.getRole());
            statement.executeUpdate();
        } catch (SQLException e) {
            System.out.println(); // TODO: 12.11.2019
            throw new IllegalArgumentException();
        }
    }

    @Override
    public void removeOne(Integer id) {
        try {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM chat.user WHERE id = ?");
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            System.out.println(); // TODO: 12.11.2019
            throw new IllegalArgumentException();
        }
    }
}

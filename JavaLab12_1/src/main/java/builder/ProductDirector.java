package builder;

import models.Product;

public class ProductDirector {
    private ProductBuilderImpl builder;

    public ProductDirector(ProductBuilderImpl builder) {
        this.builder = builder;
    }

    public Product createFullProduct(String name, String price) {
        builder.reset();
        builder.setName(name);
        builder.setPrice(price);
        return builder.getResult();
    }
}

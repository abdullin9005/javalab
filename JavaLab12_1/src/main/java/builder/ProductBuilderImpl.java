package builder;

import models.Product;

public class ProductBuilderImpl implements ProductBuilder {
    private Product product;

    @Override
    public void setName(String name) {
        product.setName(name);
    }

    @Override
    public void setPrice(String price) {
        product.setPrice(price);
    }

    @Override
    public void reset() {
        product = new Product();
    }

    @Override
    public Product getResult() {
        return product;
    }
}

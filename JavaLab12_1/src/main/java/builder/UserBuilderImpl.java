package builder;

import models.User;

public class UserBuilderImpl implements UserBuilder {
    private User user;

    @Override
    public void reset() {
        user = new User();
    }

    @Override
    public void setId(Integer id) {
        user.setId(id);
    }

    @Override
    public void setUsername(String username) {
        user.setUsername(username);
    }

    @Override
    public void setPassword(String password) {
        user.setPassword(password);
    }

    @Override
    public void setRole(String role) {
        user.setRole(role);
    }

    @Override
    public User getResult() {
        return user;
    }


}

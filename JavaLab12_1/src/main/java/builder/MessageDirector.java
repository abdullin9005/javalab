package builder;


import models.Message;

public class MessageDirector{
    private MessageBuilder builder;

    public MessageDirector(MessageBuilder builder) {
        this.builder = builder;
    }

    public Message createFullMessage(Integer writerId, String text) {
        builder.reset();
        builder.setWriterId(writerId);
        builder.setText(text);
        return builder.getResult();
    }
}

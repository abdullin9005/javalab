package builder;

import models.Message;

public interface MessageBuilder extends Builder<Message> {
    void setWriterId(Integer writerId);
    void setText(String text);
}

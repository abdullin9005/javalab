package builder;

import models.Message;

public class MessageBuilderImpl implements MessageBuilder {
    private Message message;

    @Override
    public void setWriterId(Integer writerId) {
        message.setWriterId(writerId);
    }

    @Override
    public void setText(String text) {
        message.setText(text);
    }

    @Override
    public void reset() {
        message = new Message();
    }

    @Override
    public Message getResult() {
        return message;
    }
}

package apps;

import com.beust.jcommander.JCommander;
import components.parametrs.Args;
import components.server.ChatMultiServer;
import context.ApplicationContextReflectionBased;

public class ServerLauncher {
    public static void main(String[] argv) {
        ApplicationContextReflectionBased contextReflectionBased = new ApplicationContextReflectionBased();
        Args args = new Args();
        JCommander.newBuilder()
                .addObject(args)
                .build()
                .parse(argv);
        ChatMultiServer server = (ChatMultiServer) contextReflectionBased.getComponent(ChatMultiServer.class);
        server.start(args.port);
    }
}
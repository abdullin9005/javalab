package context;

public interface ApplicationContext {
    Component getComponent(Class componentType);
}

package context;

import org.reflections.*;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class ApplicationContextReflectionBased implements ApplicationContext {

    private HashMap<Class, Object> map;

    public ApplicationContextReflectionBased() {
        this.map = getAllComponentsMap();
    }

    private HashMap<Class, Object> getAllComponentsMap() {
        try {
            return getAllComponentsMap0();
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException | InstantiationException e) {
            System.out.println();
            throw new IllegalArgumentException();
        }
    }

    private HashMap<Class, Object> getAllComponentsMap0() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Reflections reflections = new Reflections("components");
        Set<Class<? extends Component>> set = reflections.getSubTypesOf(Component.class);
        HashMap<Class, Object> map = new HashMap<>();
        for (Class<? extends Component> aClass : set) {
            map.put(aClass, aClass.getConstructor().newInstance());
        }

        for (Map.Entry<Class, Object> entry : map.entrySet()) {
            Field[] fields = entry.getKey().getDeclaredFields();
            for (Field field : fields) {
                if (set.contains(field.getType())) {
                    field.setAccessible(true);
                    field.set(entry.getValue(), map.get(field.getType()));
                }
            }
        }
        return map;
    }



    @Override
    public Component getComponent(Class componentType) {
        return (Component) map.get(componentType);
    }
}

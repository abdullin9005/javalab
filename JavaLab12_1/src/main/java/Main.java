import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.*;
import io.jsonwebtoken.security.Keys;
import models.commands.GetMessagesCommand;
import models.JsonMessage;
import org.springframework.security.authentication.AuthenticationServiceException;

import javax.crypto.SecretKey;

public class Main {
    public static void main(String[] args) throws IOException {
        /*String jwt =
                Jwts.builder().claim("id", "1")
                        .claim("role", "admin")
                        .signWith(key)
                        .compact();
        Jws<Claims> jwtText = Jwts.parser().setSigningKey(key).parseClaimsJws(jwt);
        System.out.println(jwtText.getSignature());
        System.out.println(jwt);*/

        SecretKey key = Keys.secretKeyFor(SignatureAlgorithm.HS256);

        Map<String, Object> tokenData = new HashMap<>();
        tokenData.put("userId", "1");
        tokenData.put("role", "admin");
        JwtBuilder jwtBuilder = Jwts.builder();
        jwtBuilder.setClaims(tokenData);
        String token = jwtBuilder.signWith(key).compact();

        System.out.println(0 + " " + token);

        GetMessagesCommand testMessage = new GetMessagesCommand("getMessages", 5, 5, token);
        JsonMessage jsonMessage = new JsonMessage("TestMessage", testMessage);
        ObjectMapper mapper = new ObjectMapper();
        String message = mapper.writeValueAsString(jsonMessage);

        System.out.println(1 + " " + message);

        JsonMessage jsonMessage1 = mapper.readValue(message, JsonMessage.class);
        LinkedHashMap payload = (LinkedHashMap) jsonMessage1.getPayload();
        System.out.println(2 + " " + jsonMessage1.getHeader());
        System.out.println(3 + " " + payload.get("token"));
        String token1 = (String) payload.get("token");

        Claims claims;
        try {
            claims = (Claims) Jwts.parser().setSigningKey(key).parse(token1).getBody();
        } catch (Exception ex) {
            throw new AuthenticationServiceException("Token corrupted");
        }
        System.out.println(4 + " " + claims.get("userId", String.class));
        System.out.println(5 + " " + claims.get("role", String.class));
    }
}

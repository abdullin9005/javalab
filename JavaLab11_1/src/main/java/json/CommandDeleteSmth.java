package json;

public class CommandDeleteSmth extends Payload {
    private Integer object_id;
    private String token;

    public CommandDeleteSmth(Integer object_id, String token) {
        this.object_id = object_id;
        this.token = token;
    }

    public Integer getObject_id() {
        return object_id;
    }

    public void setObject_id(Integer object_id) {
        this.object_id = object_id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}


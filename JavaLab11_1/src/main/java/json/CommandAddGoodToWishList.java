package json;

public class CommandAddGoodToWishList {
    private Integer user_id;
    private Integer good_id;
    private String token;

    public CommandAddGoodToWishList(Integer user_id, Integer good_id, String token) {
        this.user_id = user_id;
        this.good_id = good_id;
        this.token = token;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public Integer getGood_id() {
        return good_id;
    }

    public void setGood_id(Integer good_id) {
        this.good_id = good_id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}

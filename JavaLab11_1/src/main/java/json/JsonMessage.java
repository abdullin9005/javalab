package json;

public class JsonMessage {
    String Header;
    Object payload;

    public JsonMessage(String header, Object payload) {
        Header = header;
        this.payload = payload;
    }

    public JsonMessage(){
    }

    public String getHeader() {
        return Header;
    }

    public void setHeader(String header) {
        Header = header;
    }

    public Object getPayload() {
        return payload;
    }

    public void setPayload(Object payload) {
        this.payload = payload;
    }
}
package json;

import model.Message;

public class MessagesArray extends Payload {
    Message[] messages;

    public MessagesArray(Message[] messages) {
        this.messages = messages;
    }

    public Message[] getMessagesArray() {
        return messages;
    }

    public void setMessagesArray(Message[] messages) {
        this.messages = messages;
    }
}

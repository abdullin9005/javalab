package json;

public class CommandGetMessages extends Payload {
    private Integer size;
    private Integer page;
    private String token;

    public CommandGetMessages(Integer size, Integer page, String token) {
        this.size = size;
        this.page = page;
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }
}

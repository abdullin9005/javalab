package model;

import json.Payload;

public class User extends Payload {
    private String login;
    private String password_hash;
    private String role;

    public User(String login, String password_hash, String role) {
        this.login = login;
        this.password_hash = password_hash;
        this.role = role;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword_hash() {
        return password_hash;
    }

    public void setPassword_hash(String password_hash) {
        this.password_hash = password_hash;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}

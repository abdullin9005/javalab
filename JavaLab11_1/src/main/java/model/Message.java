package model;

import json.Payload;

import java.util.Date;

public class Message extends Payload {
    private String message;
    private String date;
    private Integer user_id;

    public Message(String message, String date, Integer user_id) {
        this.message = message;
        this.date = date;
        this.user_id = user_id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }
}

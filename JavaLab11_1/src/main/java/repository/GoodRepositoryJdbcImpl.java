package repository;

import model.Good;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class GoodRepositoryJdbcImpl implements CrudRepository<Good> {

    private Connection connection;
    private PasswordEncoder encoder = new BCryptPasswordEncoder();

    public GoodRepositoryJdbcImpl(String dbPropertiesFilePath) {
        this.connection = new DBConnection(dbPropertiesFilePath).getConnection();
    }

    private RowMapper<Good> goodRowMapper = rs ->
            new Good(
                    rs.getString("good"),
                    rs.getString("price")
            );

    @Override
    public Boolean save(Good good) {
        try {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO db2.goods (good, price) VALUES (?,?)");
            statement.setString(1, good.getName());
            statement.setString(2, good.getPrice());
            return statement.execute();
        } catch (SQLException e) {
            System.out.println("Error during adding good in db");
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(Good good) {

    }

    @Override
    public Good find(Good good) {
        try {

            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM db2.goods WHERE good = ? AND price = ?");
            stmt.setString(1, good.getName());
            stmt.setString(2, good.getPrice());
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return goodRowMapper.mapRow(rs);
            } else {
                return null;
            }
        } catch (SQLException e) {
            System.out.println("Error during finding good");
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void delete(Integer goodId) {
        try {
            PreparedStatement deleteGoodSql = connection.prepareStatement("DELETE FROM db2.goods WHERE idgoods = ?");
            deleteGoodSql.setInt(1, goodId);
            deleteGoodSql.executeUpdate();
            deleteGoodSql.close();
            PreparedStatement deleteGoodSql2 = connection.prepareStatement("DELETE FROM db2.good_to_user WHERE good_id = ?");
            deleteGoodSql2.setInt(1, goodId);
            deleteGoodSql2.executeUpdate();
            deleteGoodSql2.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Good> getGoods(int size, int page) {
        try {
            List<Good> list = new ArrayList<>();
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM db2.goods LIMIT ? OFFSET ?");
            statement.setInt(1, size);
            statement.setInt(2, size * page);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                list.add(goodRowMapper.mapRow(rs));
            }
            return list;
        } catch (SQLException e) {
            System.out.println("Error during getting messages from db");
            throw new IllegalArgumentException(e);
        }
    }



    @Override
    public List<Good> findAll() {
        return null;
    }
}

package repository;

import model.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class UserRepositoryJdbcImpl implements UserRepository {

    private Connection connection;
    private PasswordEncoder encoder = new BCryptPasswordEncoder();

    public UserRepositoryJdbcImpl(String dbPropertiesFilePath) {
        this.connection = new DBConnection(dbPropertiesFilePath).getConnection();
    }

    private RowMapper<User> UserRowMapper = rs ->
            new User(
                    rs.getString("login"),
                    rs.getString("password_hash"),
                    rs.getString("role")
            );

    public Integer findIdByLogin(String login) {
        String sqlQuery = "SELECT * FROM db2.user WHERE login = ?";
        try (PreparedStatement stmt = connection.prepareStatement(sqlQuery)) {
            stmt.setString(1, login);
            ResultSet rs = stmt.executeQuery();
            if (rs.next())
                return Integer.parseInt(rs.getString(1));
            else
                return null;
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    public Boolean save(User user) {
        try {
            user.setRole("user");
            PreparedStatement statement = connection.prepareStatement("INSERT INTO db2.user (login, password_hash, role) VALUES (?,?,?)");
            statement.setString(1, user.getLogin());
            String hash = hashPassword(user.getPassword_hash());
            statement.setString(2, hash);
            statement.setString(3, user.getRole());
            return statement.execute();
        } catch (SQLException e) {
            System.out.println("Error during adding user in db");
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(User user) {

    }

    public User find(User user) {
        try {

            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM db2.user WHERE login = ?");
            stmt.setString(1, user.getLogin());
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                User user1 = UserRowMapper.mapRow(rs);
                if (encoder.matches(user.getPassword_hash(), user1.getPassword_hash()))
                    return user1;
                else
                    return null;
            } else {
                return null;
            }
        } catch (SQLException e) {
            System.out.println("Error during finding user");
            throw new IllegalArgumentException(e);
        }
    }

    public void delete(Integer user_id) {
        try {
            PreparedStatement deleteGoodSql = connection.prepareStatement("DELETE FROM db2.user WHERE iduser = ?");
            deleteGoodSql.setInt(1, user_id);
            deleteGoodSql.executeUpdate();
            deleteGoodSql.close();
            PreparedStatement deleteGoodSql2 = connection.prepareStatement("DELETE FROM db2.good_to_user WHERE user_id = ?");
            deleteGoodSql2.setInt(1, user_id);
            deleteGoodSql2.executeUpdate();
            deleteGoodSql2.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<User> findAll() {
        return null;
    }

    private String hashPassword(String password) {
        //получение хэша по алгоритму bcrypt
        //полученное значение мы сохраняем в базу при регистрации
        String hash = encoder.encode(password);
        return hash;

        //проверка на соответствие пароля, присланного при попытке логина
        //с хэшем, полученным из базы данных
        //System.out.println(encoder.matches("abcdef", hash));
    }
}

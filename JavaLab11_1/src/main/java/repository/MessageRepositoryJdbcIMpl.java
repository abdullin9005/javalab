package repository;

import model.Message;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MessageRepositoryJdbcIMpl implements CrudRepository<Message> {
    private Connection connection;

    public MessageRepositoryJdbcIMpl(String dbPropertiesFilePath) {
        this.connection = new DBConnection(dbPropertiesFilePath).getConnection();
    }

    private RowMapper<Message> messageRowMapper = rs ->
            new Message(
                    rs.getString("message"),
                    rs.getString("date"),
                    rs.getInt("person_id")
            );


    @Override
    public Boolean save(Message message) {
        try {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO db2.message (message, date, user_id) VALUES (?,?,?)");
            statement.setString(1, message.getMessage());
            statement.setString(2, message.getDate());
            statement.setInt(3, message.getUser_id());
            return statement.execute();
        } catch (SQLException e) {
            System.out.println("Error during adding user in db");
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(Message message) {

    }

    @Override
    public Message find(Message message) {
        return null;
    }

    @Override
    public void delete(Integer id) {

    }

    public List<Message> getMessages(int size, int page) {
        try {
            List<Message> list = new ArrayList<>();
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM db2.message LIMIT ? OFFSET ?");
            statement.setInt(1, size);
            statement.setInt(2, size * page);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                list.add(messageRowMapper.mapRow(rs));
            }
            return list;
        } catch (SQLException e) {
            System.out.println("Error during getting messages from db");
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public List<Message> findAll() {
        try {
            List<Message> list = new ArrayList<>();
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM db2.message");
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                list.add(messageRowMapper.mapRow(rs));
            }
            return list;
        } catch (SQLException e) {
            System.out.println("Error during getting messages from db");
            throw new IllegalArgumentException(e);
        }
    }
}

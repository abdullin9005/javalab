package repository;

import model.User;

public interface UserRepository extends CrudRepository<User> {
    Integer findIdByLogin(String login);
}

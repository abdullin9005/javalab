package repository;

import model.Good;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Good_to_userRepository {
    private Connection connection;

    public Good_to_userRepository(String dbPropertiesFilePath) {
        this.connection = new DBConnection(dbPropertiesFilePath).getConnection();
    }

    private RowMapper<Good> goodRowMapper = rs ->
            new Good(
                    rs.getString("good"),
                    rs.getString("price")
            );

    public List<Good> findMyGoods(Integer user_id) {
        try {
            List<model.Good> list = new ArrayList<>();
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM db2.good_to_user WHERE user_id = ?");
            stmt.setInt(1, user_id);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                list.add(goodRowMapper.mapRow(rs));
            }
            return list;
        } catch (SQLException e) {
            System.out.println("Error during getting messages from db");
            throw new IllegalArgumentException(e);
        }
    }

    public void addGoodToUser(Integer user_id, Integer good_id){
        try {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO db2.good_to_user (user_id, good_id) VALUES (?,?)");
            statement.setInt(1, user_id);
            statement.setInt(2, good_id);
            statement.execute();
        } catch (SQLException e) {
            System.out.println("Error during adding user in db");
            throw new IllegalArgumentException(e);
        }
    }
}

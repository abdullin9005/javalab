package repository;

import java.util.List;

public interface CrudRepository<T> {
    Boolean save(T t);
    void update(T t);
    T find(T t);
    void delete(Integer id);

    List<T> findAll();
}
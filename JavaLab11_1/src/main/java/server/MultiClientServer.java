package server;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import json.CommandDeleteSmth;
import json.CommandGetAllGoods;
import json.CommandGetMessages;
import json.JsonMessage;
import model.Good;
import model.Message;
import model.User;
import org.springframework.security.authentication.AuthenticationServiceException;
import repository.GoodRepositoryJdbcImpl;
import repository.Good_to_userRepository;
import repository.MessageRepositoryJdbcIMpl;
import repository.UserRepositoryJdbcImpl;

import javax.crypto.SecretKey;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;

public class MultiClientServer {
    private ServerSocket serverSocket;
    private List<ClientHandler> clients;
    private MessageRepositoryJdbcIMpl messageRepositoryJdbc;
    private UserRepositoryJdbcImpl userRepositoryJdbc;
    private GoodRepositoryJdbcImpl goodRepositoryJdbc;
    private Good_to_userRepository good_to_userRepository;
    private SecretKey key;

    public MultiClientServer(String dbPropertiesFilePath) {
        this.messageRepositoryJdbc = new MessageRepositoryJdbcIMpl(dbPropertiesFilePath);
        this.userRepositoryJdbc = new UserRepositoryJdbcImpl(dbPropertiesFilePath);
        this.goodRepositoryJdbc = new GoodRepositoryJdbcImpl(dbPropertiesFilePath);
        this.good_to_userRepository = new Good_to_userRepository(dbPropertiesFilePath);
        this.key = Keys.secretKeyFor(SignatureAlgorithm.HS256);

        clients = new ArrayList<>();
    }

    public void start(int port) {
        try {
            serverSocket = new ServerSocket(port);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        for (; ; ) {
            try {
                ClientHandler handler =
                        new ClientHandler(serverSocket.accept());
                handler.start();
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }
    }

    private class ClientHandler extends Thread {
        private Socket clientSocket;
        private BufferedReader reader;
        private User userFromJsonString;
        private ObjectMapper mapper;

        public ClientHandler(Socket clientSocket) {
            this.clientSocket = clientSocket;
            clients.add(this);
            System.out.println("New client!");
        }

        @Override
        public void run() {
            System.out.println("in run");
            try {
                reader = new BufferedReader(
                        new InputStreamReader(
                                clientSocket.getInputStream()));
                mapper = new ObjectMapper();
                authorization();
                String line;
                String message = null;
                while ((line = reader.readLine()) != null) {
                    mapper = new ObjectMapper();
                    for (ClientHandler client : clients) {
                        if (client.clientSocket.isClosed()) {
                            clients.remove(client);
                            continue;
                        }
                        PrintWriter writer = new PrintWriter(client.clientSocket.getOutputStream(), true);
                        JsonMessage jsonMessage = mapper.readValue(line, JsonMessage.class);
                        switch (jsonMessage.getHeader()) {
                            case "Message":
                                message = (String) jsonMessage.getPayload();
                                JsonMessage jsonMessage1 = new JsonMessage();
                                jsonMessage1.setHeader("Message");
                                String string = (userFromJsonString.getLogin() + ": " + message);
                                jsonMessage1.setPayload(string);
                                mapper = new ObjectMapper();
                                String jsonValue = mapper.writeValueAsString(jsonMessage1);
                                writer.println(jsonValue);
                                break;
                            case "getMessages":
                                message = "getMessages";
                                LinkedHashMap payload = (LinkedHashMap) jsonMessage.getPayload();
                                CommandGetMessages commandGetMessages = new CommandGetMessages((Integer) payload.get("size"), (Integer) payload.get("page"), "test");//zagluha
                                List<Message> messages = messageRepositoryJdbc.getMessages(commandGetMessages.getSize(), commandGetMessages.getPage());
                                Message[] messagesArray = new Message[messages.size()];
                                messages.toArray(messagesArray);
                                JsonMessage jsonError = new JsonMessage();
                                jsonError.setHeader("Messages");
                                jsonError.setPayload(messagesArray);
                                sendMessageToClient(mapper.writeValueAsString(jsonError));
                                break;

                            case "getAllGoods":
                                message = "getAllGoods";
                                LinkedHashMap goodsPayload = (LinkedHashMap) jsonMessage.getPayload();
                                CommandGetAllGoods commandGetAllGoods = new CommandGetAllGoods((Integer) goodsPayload.get("size"), (Integer) goodsPayload.get("page"), "token");//zagluha
                                List<Good> goods = goodRepositoryJdbc.getGoods(commandGetAllGoods.getSize(), commandGetAllGoods.getPage());
                                Good[] goodsArray = new Good[goods.size()];
                                goods.toArray(goodsArray);
                                JsonMessage allGoodsJsonError = new JsonMessage();
                                allGoodsJsonError.setHeader("AllGoods");
                                allGoodsJsonError.setPayload(goodsArray);
                                sendMessageToClient(mapper.writeValueAsString(allGoodsJsonError));
                                break;

                            case "getMyGoods":
                                message = "getMyGoods";
                                List<Good> myGoods = good_to_userRepository.findMyGoods(userRepositoryJdbc.findIdByLogin(userFromJsonString.getLogin()));
                                Good[] myGoodsArray = new Good[myGoods.size()];
                                myGoods.toArray(myGoodsArray);
                                JsonMessage myGoodsJsonError = new JsonMessage();
                                myGoodsJsonError.setHeader("MyGoods");
                                myGoodsJsonError.setPayload(myGoodsArray);
                                sendMessageToClient(mapper.writeValueAsString(myGoodsJsonError));
                                break;

                            case "addGoodToWishList":
                                LinkedHashMap addTOWishListPayload = (LinkedHashMap) jsonMessage.getPayload();
                                CommandDeleteSmth commandAddToWishList = new CommandDeleteSmth((Integer) addTOWishListPayload.get("object_id"), (String) addTOWishListPayload.get("token"));
                                good_to_userRepository.addGoodToUser(userRepositoryJdbc.findIdByLogin(userFromJsonString.getLogin()), commandAddToWishList.getObject_id());
                                break;

                            case "deleteUser":
                                if (userFromJsonString.getRole().equals("admin")) {
                                    LinkedHashMap deleteUserPayload = (LinkedHashMap) jsonMessage.getPayload();
                                    String token = (String) deleteUserPayload.get("token");
                                    System.out.println(token);
                                    if (checkUserToAdmin(token)) {
                                        CommandDeleteSmth commandDeleteSmth = new CommandDeleteSmth((Integer) deleteUserPayload.get("object_id"), (String) deleteUserPayload.get("token"));
                                        System.out.println("User " + commandDeleteSmth.getObject_id() + " deleted");
                                        userRepositoryJdbc.delete(commandDeleteSmth.getObject_id());
                                    }
                                    break;
                                } else {
                                    message = (String) jsonMessage.getPayload();
                                    JsonMessage jsonMessage2 = new JsonMessage();
                                    jsonMessage2.setHeader("Message");
                                    String string1 = (userFromJsonString.getLogin() + ": " + message);
                                    jsonMessage2.setPayload(string1);
                                    mapper = new ObjectMapper();
                                    String jsonValue1 = mapper.writeValueAsString(jsonMessage2);
                                    writer.println(jsonValue1);
                                    break;
                                }

                            case "deleteGood":
                                if (userFromJsonString.getRole().equals("admin")) {
                                    LinkedHashMap deleteGoodPayload = (LinkedHashMap) jsonMessage.getPayload();
                                    String token = (String) deleteGoodPayload.get("token");
                                    if (checkUserToAdmin(token)) {
                                        CommandDeleteSmth commandDeleteGood = new CommandDeleteSmth((Integer) deleteGoodPayload.get("object_id"), (String) deleteGoodPayload.get("token"));
                                        goodRepositoryJdbc.delete(commandDeleteGood.getObject_id());
                                    }
                                    break;
                                } else {
                                    message = (String) jsonMessage.getPayload();
                                    JsonMessage jsonMessage2 = new JsonMessage();
                                    jsonMessage2.setHeader("Message");
                                    String string1 = (userFromJsonString.getLogin() + ": " + message);
                                    jsonMessage2.setPayload(string1);
                                    mapper = new ObjectMapper();
                                    String jsonValue1 = mapper.writeValueAsString(jsonMessage2);
                                    writer.println(jsonValue1);
                                    break;
                                }
                        }
                    }
                    sendMsgInDB(message);
                }
                reader.close();
                clientSocket.close();
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }

        private Boolean authorization() {
            try {
                String jsonPersonValue = reader.readLine();
                mapper = new ObjectMapper();
                JsonMessage jsonMessage = mapper.readValue(jsonPersonValue, JsonMessage.class);
                if (jsonMessage.getHeader().equals("Login")) {
                    LinkedHashMap payload = (LinkedHashMap) jsonMessage.getPayload();
                    userFromJsonString = new User((String) payload.get("login"), (String) payload.get("password_hash"), (String) payload.get("role"));
                } else {
                    userFromJsonString = null;
                }
                if (userRepositoryJdbc.find(userFromJsonString) != null) {
                    userFromJsonString = userRepositoryJdbc.find(userFromJsonString);
                    JsonMessage jsonError = new JsonMessage();
                    jsonError.setHeader("Error");
                    jsonError.setPayload(getToken(userFromJsonString));
                    sendMessageToClient(mapper.writeValueAsString(jsonError));
                    return true;
                } else {
                    if (userRepositoryJdbc.findIdByLogin(userFromJsonString.getLogin()) != null) {
                        JsonMessage jsonError = new JsonMessage();
                        jsonError.setHeader("Error");
                        jsonError.setPayload("Wrong login or password");
                        sendMessageToClient(mapper.writeValueAsString(jsonError));
                        authorization();
                        clientSocket.close();
                        return false;
                    } else {
                        userRepositoryJdbc.save(userFromJsonString);
                        JsonMessage jsonError = new JsonMessage();
                        jsonError.setHeader("Error");
                        jsonError.setPayload(getToken(userFromJsonString));
                        sendMessageToClient(mapper.writeValueAsString(jsonError));
                        return true;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            JsonMessage jsonError = new JsonMessage();
            jsonError.setHeader("Error");
            jsonError.setPayload("Wrong login or password");
            try {
                sendMessageToClient(mapper.writeValueAsString(jsonError));
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                clientSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            authorization();
            return false;
        }

        private void sendMessageToClient(String string) throws IOException {
            PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
            out.println(string);
        }

        private void sendMsgInDB(String message) {
            String login = userFromJsonString.getLogin();
            String date = (new Date()).toString();
            Message messageWithDate = new Message(message, date, userRepositoryJdbc.findIdByLogin(login));
            messageRepositoryJdbc.save(messageWithDate);
        }
    }

    private String getToken(User user) {
        Map<String, Object> tokenData = new HashMap<>();
        tokenData.put("login", user.getLogin());
        tokenData.put("role", user.getRole());
        JwtBuilder jwtBuilder = Jwts.builder();
        jwtBuilder.setClaims(tokenData);
        String token = jwtBuilder.signWith(key).compact();
        return token;
    }

    private boolean checkUserToAdmin(String token){
        Claims claims;
        try {
            claims = (Claims) Jwts.parser().setSigningKey(key).parse(token).getBody();
        } catch (Exception ex) {
            throw new AuthenticationServiceException("Token corrupted");
        }
        System.out.println(claims.get("role", String.class).equals("admin"));
        return claims.get("role", String.class).equals("admin");
    }
}



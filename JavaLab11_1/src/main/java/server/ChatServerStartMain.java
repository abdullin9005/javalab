package server;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

import java.util.ArrayList;
import java.util.List;

public class ChatServerStartMain {
    public static void main(String[] argv) {
        String dbPropertiesFilePath = "src/main/java/db.properties";
        Integer port = 60000;

        MultiClientServer multiClientServer = new MultiClientServer(dbPropertiesFilePath);
        multiClientServer.start(port);
    }
}

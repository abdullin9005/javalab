package client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import json.*;
import model.User;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Scanner;

public class ChatClient {
    private PrintWriter writer;
    private BufferedReader reader;
    private ObjectMapper mapper;
    private String token;

    public void startConnection(String ip, int port) {
        try {
            Socket clientSocket = new Socket(ip, port);
            writer = new PrintWriter(clientSocket.getOutputStream(), true);
            reader = new BufferedReader(
                    new InputStreamReader(clientSocket.getInputStream()));
            new Thread(receiveMessagesTask).start();
            authorization();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    public void sendMessage(String message) {
        String[] words = message.split(" ");
        JsonMessage jsonMessage = new JsonMessage();
        if ("Command".equals(words[0])) {
            switch (words[1]) {
                case "getMessages":
                    CommandGetMessages commandGetMessages = new CommandGetMessages(Integer.parseInt(words[2]), Integer.parseInt(words[3]), token);
                    jsonMessage.setHeader(words[1]);
                    jsonMessage.setPayload(commandGetMessages);
                    try {
                        mapper = new ObjectMapper();
                        String jsonValue = mapper.writeValueAsString(jsonMessage);
                        writer.println(jsonValue);
                    } catch (JsonProcessingException e) {
                        System.out.println("Error in mapping json object");
                        throw new IllegalArgumentException(e);
                    }
                    break;
                case "getAllGoods":
                    CommandGetAllGoods commandGetAllGoods = new CommandGetAllGoods(Integer.parseInt(words[2]), Integer.parseInt(words[3]), token);
                    jsonMessage.setHeader(words[1]);
                    jsonMessage.setPayload(commandGetAllGoods);
                    try {
                        mapper = new ObjectMapper();
                        String jsonValue = mapper.writeValueAsString(jsonMessage);
                        writer.println(jsonValue);
                    } catch (JsonProcessingException e) {
                        System.out.println("Error in mapping json object");
                        throw new IllegalArgumentException(e);
                    }
                    break;
                case "getMyGoods":
                    jsonMessage.setHeader(words[1]);
                    jsonMessage.setPayload(words[1]);
                    try {
                        mapper = new ObjectMapper();
                        String jsonValue = mapper.writeValueAsString(jsonMessage);
                        writer.println(jsonValue);

                    } catch (JsonProcessingException e) {
                        System.out.println("Error in mapping json object");
                        throw new IllegalArgumentException(e);
                    }
                    break;
                case "deleteGood":
                case "deleteUser":
                case "addGoodToWishList":
                    jsonMessage.setHeader(words[1]);
                    CommandDeleteSmth commandDeleteSmth = new CommandDeleteSmth(Integer.parseInt(words[2]), token);
                    jsonMessage.setPayload(commandDeleteSmth);
                    try {
                        mapper = new ObjectMapper();
                        String jsonValue = mapper.writeValueAsString(jsonMessage);
                        writer.println(jsonValue);
                    } catch (JsonProcessingException e) {
                        System.out.println("Error in mapping json object");
                        throw new IllegalArgumentException(e);
                    }
                    break;
            }
        } else {
            jsonMessage.setHeader("Message");
            jsonMessage.setPayload(message);
            try {
                mapper = new ObjectMapper();
                String jsonValue = mapper.writeValueAsString(jsonMessage);
                writer.println(jsonValue);

            } catch (JsonProcessingException e) {
                System.out.println("Error in mapping json object");
                throw new IllegalArgumentException(e);
            }
        }

    }

    private Runnable receiveMessagesTask = new Runnable() {
        public void run() {
            while (true) {
                String line = null;
                try {
                    line = reader.readLine();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    JsonMessage jsonMessage = mapper.readValue(line, JsonMessage.class);

                    if (jsonMessage.getHeader().equals("Messages") || jsonMessage.getHeader().equals("AllGoods") || jsonMessage.getHeader().equals("MyGoods")) {
                        ArrayList<LinkedHashMap> payload = (ArrayList<LinkedHashMap>) jsonMessage.getPayload();
                        for (int i = 0; i < payload.size(); i++) {
                            System.out.println(payload.get(i));
                        }
                    }
                    if (jsonMessage.getHeader().equals("Error"))
                        if (jsonMessage.getPayload().equals("Wrong login or password")) {
                            authorization();
                        } else {
                            token = (String) jsonMessage.getPayload();
                            //System.out.println(token);
                        }
                    else {
                        if (jsonMessage.getHeader().equals("Message")) {
                            String payload = (String) jsonMessage.getPayload();
                            System.out.println(payload);
                        }
                    }
                } catch (IOException e) {
                    System.out.println(line);
                }
            }
        }
    };

    private Boolean authorization() {
        try {
            mapper = new ObjectMapper();
            Scanner in = new Scanner(System.in);
            System.out.println("Write your nick: ");
            String login = in.nextLine();
            System.out.println("Write your password: ");
            String password = in.nextLine();
            System.out.println("Write your role: ");
            String role = in.nextLine();
            User user = new User(login, password, role);
            JsonMessage jsonPerson = new JsonMessage();
            jsonPerson.setHeader("Login");
            jsonPerson.setPayload(user);
            String jsonPersonValue = mapper.writeValueAsString(jsonPerson);
            writer.println(jsonPersonValue);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }
}

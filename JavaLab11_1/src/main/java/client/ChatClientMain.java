package client;

import com.fasterxml.jackson.core.JsonProcessingException;

import java.util.Scanner;

public class ChatClientMain {
    public static void main(String[] argv) {
        Scanner sc = new Scanner(System.in);
        ChatClient chatClient = new ChatClient();
        String serverIp = "localhost";
        int port = 60000;
        chatClient.startConnection(serverIp, port);

        while (true) {
            String message = sc.nextLine();
            chatClient.sendMessage(message);
        }
    }
}


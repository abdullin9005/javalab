package json;

import java.util.List;

public class MessagesList extends Payload {
    List messagesList;

    public MessagesList(List messagesList) {
        this.messagesList = messagesList;
    }

    public List getMessagesList() {
        return messagesList;
    }

    public void setMessagesList(List messagesList) {
        this.messagesList = messagesList;
    }
}

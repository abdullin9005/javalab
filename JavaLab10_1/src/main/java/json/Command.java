package json;

public class Command extends Payload {
    private Integer size;
    private Integer page;

    public Command(Integer size, Integer page) {
        this.size = size;
        this.page = page;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }
}

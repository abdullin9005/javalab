package repository;


import model.Person;

interface PersonRepository extends CrudRepository<Person> {
    Integer findIdByLogin(String login);
}

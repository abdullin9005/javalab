package repository;

import model.Person;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PersonRepositoryJdbcImpl implements PersonRepository {
    private Connection connection;
    private PasswordEncoder encoder = new BCryptPasswordEncoder();

    public PersonRepositoryJdbcImpl(String dbPropertiesFilePath) {
        this.connection = new DBConnection(dbPropertiesFilePath).getConnection();
    }
    private RowMapper<Person> personRowMapper = rs ->
            new Person(
                    rs.getString("login"),
                    rs.getString("password")
            );

    @Override
    public Integer findIdByLogin(String login) {
        String sqlQuery = "SELECT * FROM db1.person " +
                "WHERE login = ?";
        try (PreparedStatement stmt = connection.prepareStatement(sqlQuery)) {
            stmt.setString(1, login);
            try (ResultSet rs = stmt.executeQuery()) {
                if (rs.next())
                    return Integer.parseInt(rs.getString(1));
                else
                    return null;
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }


    }

    @Override
    public Boolean save(Person person) {
        try {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO db1.person (login, password) VALUES (?,?)");
            statement.setString(1, person.getLogin());
            statement.setString(2, person.getPassword());
            return statement.execute();
        } catch (SQLException e) {
            System.out.println("Error during adding user in db");
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(Person person) {

    }

    @Override
    public Person find(Person person) {
        try {
            String hash = hashPassword(person.getPassword());
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM db1.person WHERE login = ? AND password = ?");
            stmt.setString(1, person.getLogin());
            stmt.setString(2, hash);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return personRowMapper.mapRow(rs);
            } else {
                return null;
            }
        } catch (SQLException e) {
            System.out.println("Error during finding user");
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void delete(Person person) {

    }

    @Override
    public List<Person> findAll() {
        String sqlQuery = "SELECT * FROM db1.person";
        try (Statement stmt = connection.createStatement();
             ResultSet rs = stmt.executeQuery(sqlQuery)) {
            List<Person> users = new ArrayList<>();
            while (rs.next()) {
                users.add(personRowMapper.mapRow(rs));
            }
            return users;
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    private String hashPassword (String password){
        //получение хэша по алгоритму bcrypt
        //полученное значение мы сохраняем в базу при регистрации
        String hash = encoder.encode(password);
        return hash;

        //проверка на соответствие пароля, присланного при попытке логина
        //с хэшем, полученным из базы данных
        //System.out.println(encoder.matches("abcdef", hash));
    }
}

package server;

import com.fasterxml.jackson.databind.ObjectMapper;
import json.Command;
import json.JsonMessage;
import json.MessagesList;
import model.Message;
import model.Person;
import repository.MessageRepositoryJdbcIMpl;
import repository.PersonRepositoryJdbcImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

public class MultiClientServer {
    private ServerSocket serverSocket;
    private List<ClientHandler> clients;
    private MessageRepositoryJdbcIMpl messageRepositoryJdbc;
    private PersonRepositoryJdbcImpl personRepositoryJdbc;
    private ObjectMapper mapper;

    public MultiClientServer(String dbPropertiesFilePath) {
        this.messageRepositoryJdbc = new MessageRepositoryJdbcIMpl(dbPropertiesFilePath);
        this.personRepositoryJdbc = new PersonRepositoryJdbcImpl(dbPropertiesFilePath);
        clients = new ArrayList<>();
    }

    public void start(int port) {
        try {
            serverSocket = new ServerSocket(port);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        for (; ; ) {
            try {
                ClientHandler handler =
                        new ClientHandler(serverSocket.accept());
                handler.start();
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }
    }

    private class ClientHandler extends Thread {
        private Socket clientSocket;
        private BufferedReader reader;
        private Person personFromJsonString;
        private ObjectMapper mapper;

        public ClientHandler(Socket clientSocket) {
            this.clientSocket = clientSocket;
            clients.add(this);
            System.out.println("New client!");
        }

        @Override
        public void run() {
            System.out.println("in run");
            try {
                reader = new BufferedReader(
                        new InputStreamReader(
                                clientSocket.getInputStream()));
                mapper = new ObjectMapper();
                authorization();
                String line;
                String message;
                while ((line = reader.readLine()) != null) {
                    mapper = new ObjectMapper();
                    for (ClientHandler client : clients) {
                        if (client.clientSocket.isClosed()) {
                            clients.remove(client);
                            continue;
                        }
                        PrintWriter writer = new PrintWriter(client.clientSocket.getOutputStream(), true);
                        JsonMessage jsonMessage = mapper.readValue(line, JsonMessage.class);
                        if (jsonMessage.getHeader().equals("Message")) {
                            message = (String) jsonMessage.getPayload();
                            writer.println(personFromJsonString.getLogin() + ": " + message);
                        } else if (jsonMessage.getHeader().equals("Command")) {
                            message = "Command";
                            LinkedHashMap payload = (LinkedHashMap) jsonMessage.getPayload();
                            Command command = new Command((Integer) payload.get("size"),(Integer) payload.get("page"));
                            List<Message> messages = messageRepositoryJdbc.getMessages(command.getSize(), command.getPage());
                            MessagesList messagesList = new MessagesList(messages);
                            JsonMessage jsonError = new JsonMessage();
                            jsonError.setHeader("Messages");
                            jsonError.setPayload(messagesList);
                            sendMessageToClient(mapper.writeValueAsString(jsonError));
                        } else {
                            message = null;
                        }
                        sendMsgInDB(message);
                    }
                }
                reader.close();
                clientSocket.close();
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }

        private Boolean authorization() {
            try {
                String jsonPersonValue = reader.readLine();
                mapper = new ObjectMapper();
                JsonMessage jsonMessage = mapper.readValue(jsonPersonValue, JsonMessage.class);
                if (jsonMessage.getHeader().equals("Login")) {
                    LinkedHashMap payload = (LinkedHashMap) jsonMessage.getPayload();
                    personFromJsonString = new Person((String) payload.get("login"),(String) payload.get("password"));
                } else {
                    personFromJsonString = null;
                }
                if (personRepositoryJdbc.find(personFromJsonString) != null) {
                    JsonMessage jsonError = new JsonMessage();
                    jsonError.setHeader("Message");
                    jsonError.setPayload("It's OK!");
                    sendMessageToClient(mapper.writeValueAsString(jsonError));
                    return true;
                } else {
                    if (personRepositoryJdbc.findIdByLogin(personFromJsonString.getLogin()) != null) {
                        JsonMessage jsonError = new JsonMessage();
                        jsonError.setHeader("Error");
                        jsonError.setPayload("Wrong login or password");
                        sendMessageToClient(mapper.writeValueAsString(jsonError));
                        authorization();
                        clientSocket.close();
                        return false;
                    } else {
                        personRepositoryJdbc.save(personFromJsonString);
                        JsonMessage jsonError = new JsonMessage();
                        jsonError.setHeader("Message");
                        jsonError.setPayload("It's OK!");
                        sendMessageToClient(mapper.writeValueAsString(jsonError));
                        return true;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            JsonMessage jsonError = new JsonMessage();
            jsonError.setHeader("Error");
            jsonError.setPayload("Wrong login or password");
            try {
                sendMessageToClient(mapper.writeValueAsString(jsonError));
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                clientSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            authorization();
            return false;
        }

        private void sendMessageToClient(String string) throws IOException {
            PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
            out.println(string);
        }

        private void sendMsgInDB(String message) {
            String login = personFromJsonString.getLogin();
            Date date = new Date();
            Message messageWithDate = new Message(message, date, personRepositoryJdbc.findIdByLogin(login));
            messageRepositoryJdbc.save(messageWithDate);
        }
    }
}
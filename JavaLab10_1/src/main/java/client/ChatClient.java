package client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import json.*;
import model.Message;
import model.Person;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Scanner;

public class ChatClient {
    private Socket clientSocket;
    private PrintWriter writer;
    private BufferedReader reader;
    private Person person;
    private ObjectMapper mapper;

    public void startConnection(String ip, int port) {
        try {
            clientSocket = new Socket(ip, port);
            writer = new PrintWriter(clientSocket.getOutputStream(), true);
            reader = new BufferedReader(
                    new InputStreamReader(clientSocket.getInputStream()));
            new Thread(receiveMessagesTask).start();
            authorization();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    public void sendMessage(String message) throws JsonProcessingException {
        String[] words = message.split(" ");
        JsonMessage jsonMessage = new JsonMessage();
        if ("Command".equals(words[0])) {
            Command command = new Command(Integer.parseInt(words[2]), Integer.parseInt(words[3]));
            jsonMessage.setHeader("Command");
            jsonMessage.setPayload(command);
            try {
                mapper = new ObjectMapper();
                String jsonValue = mapper.writeValueAsString(jsonMessage);
                writer.println(jsonValue);
                String line = reader.readLine();
                JsonMessage jsonMessage1 = mapper.readValue(line, JsonMessage.class);
                if (jsonMessage1.getHeader().equals("Messages")) {
                    LinkedHashMap payload = (LinkedHashMap) jsonMessage.getPayload();
                    List<Message> messages = (List<Message>) payload.get("messagesList");
                    for (int i = 0; i < messages.size(); i++) {
                        System.out.println(messages.get(i));
                    }
                }
            } catch (JsonProcessingException e) {
                System.out.println("Error in mapping json object");
                throw new IllegalArgumentException(e);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            jsonMessage.setHeader("Message");
            jsonMessage.setPayload(message);
            try {
                mapper = new ObjectMapper();
                String jsonValue = mapper.writeValueAsString(jsonMessage);
                writer.println(jsonValue);

            } catch (JsonProcessingException e) {
                System.out.println("Error in mapping json object");
                throw new IllegalArgumentException(e);
            }
        }
    }

    private Runnable receiveMessagesTask = new Runnable() {
        public void run() {
            while (true) {
                try {
                    String message = reader.readLine();
                    System.out.println(message);
                } catch (IOException e) {
                    throw new IllegalStateException(e);
                }
            }
        }
    };

    private Boolean authorization() {
        try {
            mapper = new ObjectMapper();
            Scanner in = new Scanner(System.in);
            System.out.println("Write your nick: ");
            String login = in.nextLine();
            System.out.println("Write your password: ");
            String password = in.nextLine();
            person = new Person(login, password);
            JsonMessage jsonPerson = new JsonMessage();
            jsonPerson.setHeader("Login");
            jsonPerson.setPayload(person);
            String jsonPersonValue = mapper.writeValueAsString(jsonPerson);
            writer.println(jsonPersonValue);

            String jsonError = in.nextLine();
            JsonMessage errorFromJsonString = mapper.readValue(jsonError, JsonMessage.class);
            String errorString = (String) errorFromJsonString.getPayload();
            if (errorFromJsonString.getHeader().equals("Error") && errorString.equals("Wrong login or password")){
                authorization();
            } else if (errorFromJsonString.getHeader().equals("Message") && errorString.equals("It's OK!"))
                return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }
}

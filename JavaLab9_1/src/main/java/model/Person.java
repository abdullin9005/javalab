package model;

public class Person {

    private String login;
    private String password;

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public Person(String login, String password) {

        this.login = login;
        this.password = password;
    }
}

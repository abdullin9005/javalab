package model;

import java.util.Date;

public class Message {
    private String message;
    private Date date;
    private Integer person_id;

    public Message(String message, Date date, Integer person_id) {
        this.message = message;
        this.date = date;
        this.person_id = person_id;
    }

    public String getMessage() {
        return message;
    }

    public Date getDate() {
        return date;
    }

    public Integer getPerson_id() {
        return person_id;
    }
}

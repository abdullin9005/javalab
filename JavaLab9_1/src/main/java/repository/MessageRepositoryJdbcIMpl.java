package repository;

import model.Message;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class MessageRepositoryJdbcIMpl implements CrudRepository<Message>  {
    private Connection connection;

    public MessageRepositoryJdbcIMpl(String dbPropertiesFilePath) {
        this.connection = new DBConnection(dbPropertiesFilePath).getConnection();
    }
    private RowMapper<Message> messageRowMapper = rs ->
            new Message(
                    rs.getString("message"),
                    rs.getDate("date"),
                    rs.getInt("person_id")
            );


    @Override
    public Boolean save(Message message) {
        try {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO db1.message (message, date, person_id) VALUES (?,?,?)");
            statement.setString(1, message.getMessage());
            statement.setString(2, message.getDate().toString());
            statement.setInt(3, message.getPerson_id());
            return statement.execute();
        } catch (SQLException e) {
            System.out.println("Error during adding user in db");
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(Message message) {

    }

    @Override
    public Message find(Message message) {
        return null;
    }

    @Override
    public void delete(Message message) {

    }

    @Override
    public List findAll() {
        return null;
    }
}

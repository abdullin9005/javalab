package client;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ChatClientMain2 {
    public static void main(String[] argv) {
        Scanner sc = new Scanner(System.in);
        ChatClient chatClient = new ChatClient();
        String serverIp = "localhost";
        Integer port = 6000;
        chatClient.startConnection(serverIp, port);

        while (true) {
            String message = sc.nextLine();
            chatClient.sendMessage(message);
        }
    }
}


package server;

import model.Message;
import model.Person;
import repository.MessageRepositoryJdbcIMpl;
import repository.PersonRepositoryJdbcImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MultiClientServer {
    private ServerSocket serverSocket;
    private List<ClientHandler> clients;
    private MessageRepositoryJdbcIMpl messageRepositoryJdbc;
    private PersonRepositoryJdbcImpl personRepositoryJdbc;

    public MultiClientServer(String dbPropertiesFilePath) {
        this.messageRepositoryJdbc = new MessageRepositoryJdbcIMpl(dbPropertiesFilePath);
        this.personRepositoryJdbc = new PersonRepositoryJdbcImpl(dbPropertiesFilePath);
        clients = new ArrayList<>();
    }

    public void start(int port) {
        try {
            serverSocket = new ServerSocket(port);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        for (; ; ) {
            try {
                ClientHandler handler =
                        new ClientHandler(serverSocket.accept());
                handler.start();
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }
    }

    private class ClientHandler extends Thread {
        private Socket clientSocket;
        private BufferedReader reader;
        private Person person;

        public ClientHandler(Socket clientSocket) {
            this.clientSocket = clientSocket;
            clients.add(this);
            System.out.println("New client!");
        }

        @Override
        public void run() {
            System.out.println("in run");
            try {
                reader = new BufferedReader(
                        new InputStreamReader(
                                clientSocket.getInputStream()));
                if (!authorization(reader)){
                    sendMessageToClient("Wrong login or password");
                    clientSocket.close();
                }
                String line;
                while ((line = reader.readLine()) != null) {

                    for (ClientHandler client : clients) {
                        if (client.clientSocket.isClosed()) {
                            clients.remove(client);
                            continue;
                        }
                        PrintWriter writer = new PrintWriter(client.clientSocket.getOutputStream(), true);
                        writer.println(person.getLogin() + ": " + line);
                        sendMsgInDB(line);
                    }
                }
                reader.close();
                clientSocket.close();
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }

        private Boolean authorization(BufferedReader reader) {
                try {
                    sendMessageToClient("Write your nick: ");
                    String login = reader.readLine();
                    sendMessageToClient("Write your password: ");
                    String password = reader.readLine();
                    person = new Person(login, password);
                    if (personRepositoryJdbc.find(person) != null) {
                        return true;
                    } else {
                        if (personRepositoryJdbc.findIdByLogin(person.getLogin()) != null){
                            authorization(reader);
                        } else {
                            personRepositoryJdbc.save(person);
                            return true;
                        }
                            return false;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            return false;
        }

        private void sendMessageToClient(String string) throws IOException {
            PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
            out.println("SERVER: " + string);
        }

        private void sendMsgInDB(String message) {
            String login = person.getLogin();
            Date date = new Date();
            Message messageWithDate = new Message(message, date, personRepositoryJdbc.findIdByLogin(login));
            messageRepositoryJdbc.save(messageWithDate);
        }
    }
}